import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
//import {TextComponent} from './components/text/text.component';/* components use like this*/
const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./components/home/home.module').then((m) => m.HomePageModule),/* pages use like this*/
  },

  {
    path: 'explore',
    loadChildren: () =>
      import('./components/explore/explore.module').then(
        (m) => m.ExplorePageModule
      ),
  },
  {
    path: 'account',
    loadChildren: () =>
      import('./components/account/account/account.module').then(
        (m) => m.AccountPageModule
      ),
  },
  {
    path: 'groups',
    loadChildren: () =>
      import('./components/groups/groups/groups.module').then(
        (m) => m.GroupsPageModule
      ),
  },
  {
    path: 'auction',
    loadChildren: () =>
      import('./components/auctions/auction/auction.module').then(
        (m) => m.AuctionPageModule
      ),
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
