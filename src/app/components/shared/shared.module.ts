

import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {NavComponent} from './nav/nav.component';
import {SearchNavComponent} from './search-nav/search-nav.component';
import {TopNavComponent} from './top-nav/top-nav.component';
import {StoryComponent} from './story/story.component';
import {CardComponent} from './card/card.component';
import {BoxComponent} from './box/box.component';
import {LiveComponent} from './live/live.component';
@NgModule({
  declarations: [
    NavComponent,SearchNavComponent,TopNavComponent,StoryComponent,CardComponent,BoxComponent,LiveComponent


  ],
  imports: [

  ],
  exports: [
     NavComponent,SearchNavComponent,TopNavComponent,StoryComponent,CardComponent,BoxComponent,LiveComponent
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SharedModule {

}

