import { NgModule , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {SearchNavComponent} from '../shared/search-nav/search-nav.component';
import { IonicModule } from '@ionic/angular';
import {BoxComponent} from '../shared/box/box.component';
import { ExplorePageRoutingModule } from './explore-routing.module';
import {CardComponent} from '../shared/card/card.component';
import { ExplorePage } from './explore.page';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExplorePageRoutingModule
  ],
  declarations: [ExplorePage,SearchNavComponent,BoxComponent,CardComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ExplorePageModule {}
