import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {SearchNavComponent} from '../../shared/search-nav/search-nav.component';
import { IonicModule } from '@ionic/angular';
import {LiveComponent} from '../../shared/live/live.component';
import { AuctionPageRoutingModule } from './auction-routing.module';
import {BoxComponent} from '../../shared/box/box.component';
import { AuctionPage } from './auction.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AuctionPageRoutingModule
  ],
  declarations: [AuctionPage,SearchNavComponent,LiveComponent,BoxComponent]
})
export class AuctionPageModule {}
