import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {TopNavComponent} from '../shared/top-nav/top-nav.component';
import {StoryComponent} from '../shared/story/story.component';
import {CardComponent} from '../shared/card/card.component';
import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';


import { HomePage } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage,TopNavComponent,StoryComponent,CardComponent]
})
export class HomePageModule {}
